package mx.ihsa.android.mybank.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.common.base.Strings;

import mx.ihsa.android.mybank.R;
import mx.ihsa.android.mybank.model.Usuario;
import mx.ihsa.android.mybank.repository.RepositoryFactory;
import mx.ihsa.android.mybank.repository.UsuarioRepository;

public class RegistrarActivity extends AppCompatActivity {

    private EditText txtNombre;
    private EditText txtClave;
    private EditText txtCorreo;
    private EditText txtDireccion;
    private ProgressBar pbRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        bind();
    }

    private void bind() {
        txtNombre = findViewById(R.id.txtNombre);
        txtClave = findViewById(R.id.txtClaveRegistro);
        txtCorreo = findViewById(R.id.txtCorreoRegistro);
        txtDireccion = findViewById(R.id.txtDireccion);
        pbRegistrar = findViewById(R.id.pb_registrar);

        pbRegistrar.setVisibility(View.GONE);
        pbRegistrar.setIndeterminate(false);
    }

    public void guardar(View view) {
        if(Strings.isNullOrEmpty(txtNombre.getText().toString())
            || Strings.isNullOrEmpty(txtCorreo.getText().toString())
            || Strings.isNullOrEmpty(txtClave.getText().toString())) {
            mostrarToast("Debe proporcionar nombre, correo y contraseña para poder registrarse.");
        } else {
            new DataSaver().execute();
        }
    }


    private void mostrarToast(String mensaje) {
        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        finishAndRemoveTask();
    }


    private void cambiarEstatus(boolean estatus) {
        txtCorreo.setEnabled(estatus);
        txtClave.setEnabled(estatus);
        txtDireccion.setEnabled(estatus);
        txtNombre.setEnabled(estatus);
    }

    private class DataSaver extends AsyncTask<Void, Void, Void> {

        boolean registrado = false;

        @Override
        protected void onPreExecute() {
            pbRegistrar.setIndeterminate(true);
            pbRegistrar.setVisibility(View.VISIBLE);

            cambiarEstatus(false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            UsuarioRepository repo = RepositoryFactory.getUsuarioRepository();

            Usuario usuario =
                    Usuario.builder()
                            .nombre(txtNombre.getText().toString())
                            .correo(txtCorreo.getText().toString())
                            .clave(txtClave.getText().toString())
                            .direccion(txtDireccion.getText().toString())
                            .build();

            registrado = repo.guardar(getApplicationContext(), usuario);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            pbRegistrar.setIndeterminate(false);
            pbRegistrar.setVisibility(View.GONE);

            if (registrado) {
                mostrarToast("El usuario fue registrado exitosamente.");
            } else {
                mostrarToast("El usuario no fue registrado.");
            }

            cambiarEstatus(true);
        }
    }
}
