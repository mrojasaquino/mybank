package mx.ihsa.android.mybank.data;

import mx.ihsa.android.mybank.model.Cuenta;
import mx.ihsa.android.mybank.model.Movimiento;
import mx.ihsa.android.mybank.model.Usuario;

public class UtilsDB {


    public static final String TEXT_TYPE = " TEXT";
    public static final String DATE_TYPE = " DATE";
    public static final String NUMERIC_TYPE = " NUMERIC";
    public static final String INTEGER_TYPE = " INTEGER";


    public static final String CREATE_TABLE_USUARIO =
            "CREATE TABLE " + Usuario.NOMBRE_TABLA + " (\n" +
                    Usuario.ID + " " + TEXT_TYPE + ", \n" +
                    Usuario.NOMBRE + " " + TEXT_TYPE + ", \n" +
                    Usuario.CLAVE + " " + TEXT_TYPE  + ", \n" +
                    Usuario.DIRECCION + " " + TEXT_TYPE  + ", \n" +
                    Usuario.CORREO + " " + TEXT_TYPE  + "\n" +
                    ")";

    public static final String DROP_TABLE_USUARIO =
            "DROP TABLE IF EXISTS " + Usuario.NOMBRE_TABLA;

    public static final String CREATE_TABLE_MOVIMIENTO =
            "CREATE TABLE " + Movimiento.NOMBRE_TABLA + "(\n" +
                Movimiento.ID + " " + TEXT_TYPE + ", \n" +
                    Movimiento.USUARIO_ID + " " + INTEGER_TYPE + ", \n" +
                    Movimiento.FECHA + DATE_TYPE + " , \n" +
                    Movimiento.CANTIDAD + NUMERIC_TYPE + " , \n" +
                    Movimiento.TIPO + INTEGER_TYPE + " , \n" +
                    "FOREIGN KEY (" + Movimiento.USUARIO_ID +
                    ") REFERENCES " + Usuario.NOMBRE_TABLA + "(" + Usuario.ID +") \n" +
                    ")";

    public static final String DROP_TABLE_MOVIMIENTO =
            "DROP TABLE IF EXISTS " + Movimiento.NOMBRE_TABLA;

    public static final String CREATE_TABLE_CUENTA =
            "CREATE TABLE " + Cuenta.NOMBRE_TABLA + " (\n"
            + Cuenta.ID + " " + TEXT_TYPE + ", \n"
            + Cuenta.USUARIO_ID + " " + TEXT_TYPE + ", \n"
            + Cuenta.NUMERO + " " + TEXT_TYPE + ", \n"
            + Cuenta.SALDO + " " + NUMERIC_TYPE + " \n"
            + ")";

    public static final String DROP_TABLE_CUENTA =
            "DROP TABLE IF EXISTS " + Cuenta.NOMBRE_TABLA;


    private UtilsDB(){}
}
