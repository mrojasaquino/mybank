package mx.ihsa.android.mybank.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import mx.ihsa.android.mybank.R;
import mx.ihsa.android.mybank.model.Movimiento;
import mx.ihsa.android.mybank.model.TipoMovimiento;
import mx.ihsa.android.mybank.model.Usuario;
import mx.ihsa.android.mybank.repository.CuentaRepository;
import mx.ihsa.android.mybank.repository.MovimientoRepository;
import mx.ihsa.android.mybank.repository.RepositoryFactory;

public class PagoActivity extends AppCompatActivity {


    private Spinner spnCuenta;
    private Spinner spnTipoPago;
    private EditText etMonto;
    private EditText etConcepto;
    private Button btnGuardarMonto;
    private Button btnCancelarMonto;
    private ProgressBar pbGuardaPago;

    private Bundle bundle;
    private Context ctx;

    private Usuario usuario;

    private String cuentaId;

    private CuentaRepository ctaRepo;

    private String[] tipoPagos = {"Luz", "Agua", "Telcel", "AT&T"};
    private String tipoPago;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pago);


        init();
        loadData();
    }

    private void init() {
        spnCuenta = findViewById(R.id.spnCuenta);
        spnCuenta.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        cuentaId = usuario.getCuentas().get(spnCuenta.getSelectedItemPosition()).getId();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );

        spnTipoPago = findViewById(R.id.spn_tipo_pago);
        spnTipoPago.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        tipoPago = spnTipoPago.getSelectedItem().toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );




        etMonto = findViewById(R.id.et_monto);
        etConcepto = findViewById(R.id.et_concepto);


        btnGuardarMonto = findViewById(R.id.btn_guardar_monto);
        btnGuardarMonto.setOnClickListener(l -> guardarPago());

        btnCancelarMonto = findViewById(R.id.btn_cancelar_monto);
        btnCancelarMonto.setOnClickListener(l -> cancelar());

        pbGuardaPago = findViewById(R.id.pb_guarda_pago);
        pbGuardaPago.setVisibility(View.GONE);

        ctaRepo = RepositoryFactory.getCuentaRepository();
    }


    private void loadData() {
        bundle = this.getIntent().getExtras();
        usuario = (Usuario) bundle.get(Usuario.NOMBRE_TABLA);
        ctx = getApplicationContext();

        usuario.setCuentas(ctaRepo.getCuentasForUsuario(ctx, usuario.getId()));

        if (usuario.getCuentas().isEmpty()) {
            mostrarToast("El usuario no tiene cuentas.");
        } else {
            AtomicReference<List<String>> elementos = new AtomicReference<>(new ArrayList<>());
            usuario.getCuentas().forEach(c -> elementos.get().add(c.getNumero()));

            if (elementos.get() != null) {
                ArrayAdapter<String> adapter =
                        new ArrayAdapter<>(ctx, android.R.layout.simple_spinner_item, elementos.get());
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnCuenta.setAdapter(adapter);
                spnCuenta.setSelection(0);

                cuentaId = usuario.getCuentas().get(0).getId();
            }

            ArrayAdapter<String> adapterTP =
                    new ArrayAdapter<>(ctx, android.R.layout.simple_spinner_item, tipoPagos);
            adapterTP.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnTipoPago.setAdapter(adapterTP);
            spnTipoPago.setSelection(0);

        }
    }

    private void guardarPago() {
        if (TextUtils.isEmpty(etMonto.getText())
                || TextUtils.isEmpty(etConcepto.getText())) {
            mostrarToast("Debe introducir el monto y el concepto del pago.");
        } else if(Double.valueOf(etMonto.getText().toString()) > 1000){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Alerta por monto")
                    .setMessage("El monto del pago es superior a 1000")
                    .setCancelable(true)
                    .setNeutralButton("Aceptar", (dialog, which) -> new PagoSaver().execute())
                    .show();
        } else {
            new PagoSaver().execute();
        }
    }

    private void cancelar() {
        spnCuenta.setSelection(0);
        etMonto.setText(null);
    }


    private void cambiarEstatus(boolean estado) {
        spnCuenta.setEnabled(estado);
        etMonto.setEnabled(estado);
        etConcepto.setEnabled(estado);
        btnGuardarMonto.setEnabled(estado);
        btnCancelarMonto.setEnabled(estado);
    }


    private void mostrarToast(String mensaje) {
        Toast.makeText(ctx, mensaje, Toast.LENGTH_SHORT).show();
    }


    private void limpiar() {
        spnCuenta.setSelection(0);
        etConcepto.setText(null);
        etMonto.setText(null);
    }

    @Override
    protected void onStop() {
        super.onStop();
        finishAndRemoveTask();
    }


    private class PagoSaver extends AsyncTask<Void, Void, Void> {

        private boolean registrado;

        @Override
        protected void onPreExecute() {
            pbGuardaPago.setIndeterminate(true);
            pbGuardaPago.setVisibility(View.VISIBLE);

            cambiarEstatus(false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            MovimientoRepository repo = RepositoryFactory.getMovimientoRepository();


            Movimiento pago =
                    Movimiento.builder()
                            .cuentaId(cuentaId)
                            .usuarioId(usuario.getId())
                            .cantidad(Double.valueOf(etMonto.getText().toString()) * -1)
                            .concepto(etConcepto.getText().toString())
                            .tipo(TipoMovimiento.PAGO.getId())
                            .tipoPago(tipoPago)
                            .fecha(new Date(System.currentTimeMillis()))
                            .build();

            registrado = repo.guardarMovimiento(ctx, pago);

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            pbGuardaPago.setIndeterminate(false);
            pbGuardaPago.setVisibility(View.GONE);

            cambiarEstatus(true);

            if (registrado) {
                mostrarToast("El pago fue registrado exitosamente.");
                limpiar();
                finish();
            } else {
                mostrarToast("El pago no fue registrado.");
            }
        }
    }
}
