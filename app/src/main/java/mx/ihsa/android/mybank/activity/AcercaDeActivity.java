package mx.ihsa.android.mybank.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import mx.ihsa.android.mybank.R;

public class AcercaDeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acerca_de);
    }


    @Override
    protected void onStop() {
        super.onStop();
        finishAndRemoveTask();
    }
}
