package mx.ihsa.android.mybank.repository;

import android.content.Context;

import mx.ihsa.android.mybank.model.Cuenta;
import mx.ihsa.android.mybank.model.Usuario;

public abstract class UsuarioRepository {


    public abstract Usuario recuperar(Context ctx, String correo, String clave);

    public abstract boolean guardar(Context ctx, Usuario usuario);

    public abstract boolean agregarCuenta(Context ctx, Cuenta cuenta);
}
