package mx.ihsa.android.mybank.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Cuenta implements Serializable {

    public static final String ID = "id";
    public static final String USUARIO_ID = "usuarioId";
    public static final String NUMERO = "numero";
    public static final String SALDO = "saldo";

    public static final String NOMBRE_TABLA = "cuenta";


    private String id;
    private String usuarioId;
    private String numero;
    private double saldo;

    public Cuenta(){}

    public Cuenta(String id, String usuarioId, String numero, double saldo) {
        this.id = id;
        this.usuarioId = usuarioId;
        this.numero = numero;
        this.saldo = saldo;
    }


    @Override
    public String toString() {
        return "Cuenta{" +
                "id='" + id + '\'' +
                ", usuarioId='" + usuarioId + '\'' +
                ", numero='" + numero + '\'' +
                ", saldo=" + saldo +
                '}';
    }
}
