package mx.ihsa.android.mybank.repository.firebase;

import android.content.Context;
import android.os.SystemClock;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import mx.ihsa.android.mybank.model.Cuenta;
import mx.ihsa.android.mybank.model.Usuario;

public class UsuarioRepository extends mx.ihsa.android.mybank.repository.UsuarioRepository {

    private static final String TAG = "*** UsuarioRepository ";

    private FirebaseFirestore firebaseFirestore;

    @Override
    public Usuario recuperar(Context ctx, String correo, String clave) {
        final AtomicReference<Usuario> refUsuario = new AtomicReference<>();
        Usuario retVal = null;

        firebaseFirestore = FirebaseFirestore.getInstance();

        Task<QuerySnapshot> task = firebaseFirestore.collection(Usuario.NOMBRE_TABLA)
                .whereEqualTo(Usuario.CORREO, correo)
                .whereEqualTo(Usuario.CLAVE, clave)
                .get();


        while (!task.isSuccessful()) {
            SystemClock.sleep(5);
        }

        if (!task.getResult().getDocuments().isEmpty()) {
            retVal = task.getResult().getDocuments().get(0).toObject(Usuario.class);
            //retVal.setId(task.getResult().getDocuments().get(0).getId());

            task = firebaseFirestore
                    .collection(Usuario.NOMBRE_TABLA + "/" + retVal.getId() + "/" + Cuenta.NOMBRE_TABLA)
//                    .whereEqualTo(Cuenta.USUARIO_ID, retVal.getId())
                    .get();

            while (!task.isSuccessful()) {
                SystemClock.sleep(5);
            }

            if (task.getResult().getDocuments().isEmpty()) {
                retVal.setCuentas(Collections.emptyList());
            } else {
                retVal.setCuentas(
                        task.getResult().getDocuments().stream()
                                .map(t -> t.toObject(Cuenta.class))
                                .collect(Collectors.toList())
                );
            }

        }

        return retVal;
    }

    @Override
    public boolean guardar(Context ctx, Usuario usuario) {
        AtomicReference<Boolean> guardado = new AtomicReference<>(Boolean.FALSE);

        usuario.setId(UUID.randomUUID().toString());

        firebaseFirestore = FirebaseFirestore.getInstance();

        DocumentReference docUsr =
                firebaseFirestore.collection(Usuario.NOMBRE_TABLA)
                        .document(usuario.getId());
        //.set(usuario);


        Cuenta cuenta1 = new Cuenta().builder()
                .id(UUID.randomUUID().toString())
                .usuarioId(usuario.getId())
                .numero("Cuenta 1 - " + usuario.getCorreo())
                .saldo(1000)
                .build();

        DocumentReference docCta1 =
                firebaseFirestore
                        .collection(Cuenta.NOMBRE_TABLA)
                        .document(cuenta1.getId());

        Cuenta cuenta2 = new Cuenta().builder()
                .id(UUID.randomUUID().toString())
                .usuarioId(usuario.getId())
                .numero("Cuenta 2 - " + usuario.getCorreo())
                .saldo(1000)
                .build();

        DocumentReference docCta2 =
                firebaseFirestore
                        .collection(Cuenta.NOMBRE_TABLA)
                        .document(cuenta2.getId());


        Task task =
                firebaseFirestore.runTransaction(t -> {
                    t.set(docUsr, usuario);
                    t.set(docCta1, cuenta1);
                    t.set(docCta2, cuenta2);

                    return null;
                }).addOnSuccessListener(l -> guardado.set(Boolean.TRUE));

        while (!task.isSuccessful()) {
            SystemClock.sleep(5);
        }

        return guardado.get();
    }


    @Override
    public boolean agregarCuenta(Context ctx, Cuenta cuenta) {
        AtomicReference<Boolean> guardado = new AtomicReference<>(Boolean.FALSE);
        firebaseFirestore = FirebaseFirestore.getInstance();

        Task task =
                firebaseFirestore
                        .collection(Cuenta.NOMBRE_TABLA)
                        .document(cuenta.getId())
                        .set(cuenta)
                        .addOnSuccessListener(t -> guardado.set(Boolean.TRUE));

        while (!task.isSuccessful()) {
            SystemClock.sleep(5);
        }

        return guardado.get();
    }

}
