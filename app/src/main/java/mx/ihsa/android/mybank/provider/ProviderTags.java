package mx.ihsa.android.mybank.provider;

import android.net.Uri;

import mx.ihsa.android.mybank.model.Movimiento;
import mx.ihsa.android.mybank.model.Usuario;

public class ProviderTags {

    public static final String AUTHORITY = "mx.ihsa.android.mybank.provider";

    public static final String URI_STRING_USUARIO = "content://" + AUTHORITY + '/' + Usuario.NOMBRE_TABLA;
    public static final Uri URI_CONTENT_USUARIO = Uri.parse(URI_STRING_USUARIO);

    public static final String URI_STRING_MOVIMIENTO = "content://" + AUTHORITY + '/' + Movimiento.NOMBRE_TABLA;
    public static final Uri URI_CONTENT_MOVIMIENTO = Uri.parse(URI_STRING_MOVIMIENTO);

    public static final String TAG = "*** PROVIDER ";


    private ProviderTags(){}
}
