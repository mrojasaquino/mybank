package mx.ihsa.android.mybank.repository.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import mx.ihsa.android.mybank.data.DBHelper;
import mx.ihsa.android.mybank.model.Movimiento;
import mx.ihsa.android.mybank.model.TipoMovimiento;

public class MovimientoRepository {

    private static final String TAG = "*** MovimientoRepository ";



    public static Map<TipoMovimiento, Double> getTotalesParaUsuario(Context ctx, String usuarioId) {
        Map<TipoMovimiento, Double> retVal = null;

        final DBHelper dbHelper = new DBHelper(ctx);
        final String[] proyeccion = new String[]{Movimiento.TIPO, "sum(cantidad)", "count(*)"};
        final String seleccion = Movimiento.USUARIO_ID + " = ?";
        final String[] argumentos = new String[]{usuarioId};

        Log.i(TAG, "Cargando movimientos para usuario : " + usuarioId);

        try (
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                Cursor cursor =
                        db.query(
                                Movimiento.NOMBRE_TABLA,
                                proyeccion,
                                seleccion,
                                argumentos,
                                Movimiento.TIPO,
                                null,
                                null
                        );
        ) {
            retVal = new HashMap<>();
            while (cursor.moveToNext()) {
                Log.i(TAG, "Usuario : " + usuarioId + ", tipo : " + cursor.getInt(0) + ", registros : " + cursor.getDouble(2));
                retVal.put(TipoMovimiento.valueOf(cursor.getInt(0)), cursor.getDouble(1));
            }
        } catch (SQLiteException e) {
            Log.w(TAG, e);
        }

        if (retVal == null) {
            retVal = Collections.emptyMap();
        }

        return retVal;
    }


    public static boolean guardarMovimiento(Context ctx, Movimiento movimiento) {
        boolean retVal = false;

        return retVal;
    }


    public static boolean transferirMonto(Context ctx, Movimiento origen, Movimiento destino) {
        boolean retVal = false;

        return retVal;
    }


    private static Map<TipoMovimiento, Double> getTipoMovimientoFromDB(Context ctx, String usuarioId) {
        Map<TipoMovimiento, Double> retVal = null;


        return retVal;
    }



    private MovimientoRepository() {
    }
}
