package mx.ihsa.android.mybank.repository;

import android.content.Context;

import java.util.Map;

import mx.ihsa.android.mybank.model.Movimiento;
import mx.ihsa.android.mybank.model.TipoMovimiento;

public abstract class MovimientoRepository {

    public abstract Map<TipoMovimiento, Double> getTotalesParaUsuario(Context ctx, String usuarioId);

    public abstract boolean transferirMonto(Context ctx, Movimiento origen, Movimiento destino);

    public abstract boolean guardarMovimiento(Context ctx, Movimiento movimiento);
}
