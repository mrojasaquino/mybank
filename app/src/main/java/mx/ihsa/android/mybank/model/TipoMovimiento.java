package mx.ihsa.android.mybank.model;

import java.util.HashMap;
import java.util.Map;

public enum TipoMovimiento {
    PAGO(1),
    TRANSFERENCIA(2);

    private int id;

    private static Map<Integer, TipoMovimiento> map = new HashMap<>();

    private TipoMovimiento(int id) {
        this.id = id;
    }

    static {
        for(TipoMovimiento tipo : TipoMovimiento.values()) {
            map.put(tipo.getId(), tipo);
        }
    }


    public int getId() {
        return id;
    }

    public static TipoMovimiento valueOf(int id) {
        return map.get(id);
    }
}
