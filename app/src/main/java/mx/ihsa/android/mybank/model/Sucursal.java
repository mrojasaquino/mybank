package mx.ihsa.android.mybank.model;

import com.google.firebase.firestore.GeoPoint;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class Sucursal {

    public static final String NOMBRE = "nombre";
    public static final String DIRECCION = "direccion";
    public static final String UBICACION = "ubicacion";

    public static final String NOMBRE_TABLA = "sucursal";


    private String nombre;
    private String direccion;
    private GeoPoint ubicacion;

    public Sucursal(){};


    public Sucursal(String nombre, String direccion, GeoPoint ubicacion) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.ubicacion = ubicacion;
    }
}
