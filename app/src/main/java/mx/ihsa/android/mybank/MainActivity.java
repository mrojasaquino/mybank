package mx.ihsa.android.mybank;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import mx.ihsa.android.mybank.activity.BienvenidaActivity;
import mx.ihsa.android.mybank.activity.RegistrarActivity;
import mx.ihsa.android.mybank.model.Usuario;
import mx.ihsa.android.mybank.repository.RepositoryFactory;
import mx.ihsa.android.mybank.repository.UsuarioRepository;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "*** MainActivity | ";


    private EditText txtCorreo;
    private EditText txtClave;
    private Button btnIngresar;
    private Button btnRegistrar;
    private ProgressBar progressBar;

    private Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();
    }

    private void bind() {
        txtCorreo = findViewById(R.id.txtCorreo);
        txtClave = findViewById(R.id.txtClave);

        btnIngresar = findViewById(R.id.btnIngresar);
        btnRegistrar = findViewById(R.id.btnRegistrar);

        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);

        ctx = getApplicationContext();
    }

    public void registrar(View view) {
        txtCorreo.setText(null);
        txtClave.setText(null);

        Intent intent = new Intent(ctx, RegistrarActivity.class);
        startActivity(intent);
    }

    public void ingresar(View view) {
        if(txtCorreo.getText().toString() == null || txtClave.getText().toString() == null) {
            mostrarToast("Debe proporcionar el nombre de usuario y la contraseña.");
        } else {
            new DataVerifier().execute();
        }
    }


    private void lanzarActividad(Usuario usuario) {
        if(usuario == null) {
            mostrarToast("No se encontró al usuario.");
        } else {

            txtCorreo.setText(null);
            txtClave.setText(null);

            Intent intent = new Intent(ctx, BienvenidaActivity.class);
            intent.putExtra(Usuario.NOMBRE_TABLA, usuario);
            startActivity(intent);

        }
    }


    private void mostrarToast(String mensaje) {
        Toast.makeText(ctx, mensaje, Toast.LENGTH_SHORT).show();
    }


    private void cambiarEstatus(boolean estatus) {
        txtCorreo.setEnabled(estatus);
        txtClave.setEnabled(estatus);
        btnIngresar.setEnabled(estatus);
        btnRegistrar.setEnabled(estatus);
    }



    private class DataVerifier extends AsyncTask<Void, Void, Void> {

        private Usuario usuario;

        @Override
        protected void onPreExecute() {
            progressBar.setIndeterminate(true);
            progressBar.setVisibility(View.VISIBLE);

            cambiarEstatus(false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            UsuarioRepository repo = RepositoryFactory.getUsuarioRepository();

            usuario =
                    repo.recuperar(
                            ctx,
                            txtCorreo.getText().toString(),
                            txtClave.getText().toString()
                    );

            return null;
        }


        @Override
        protected void onPostExecute(Void value) {
            progressBar.setIndeterminate(false);
            progressBar.setVisibility(View.GONE);

            cambiarEstatus(true);

            lanzarActividad(usuario);
        }
    }
}
