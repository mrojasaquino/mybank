package mx.ihsa.android.mybank.model;

import java.sql.Timestamp;
import java.util.Date;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Movimiento {

    public static final String NOMBRE_TABLA = "movimiento";

    public static final String ID = "id";
    public static final String USUARIO_ID = "usuario_id";
    public static final String CUENTA_ID = "cuenta_id";
    public static final String FECHA = "fecha";
    public static final String CANTIDAD = "cantidad";
    public static final String TIPO = "tipo";

    private String id;
    private String usuarioId;
    private String cuentaId;
    private Date fecha;
    private double cantidad;
    private int tipo;
    private String concepto;
    private String tipoPago;


    public Movimiento(){}

    public Movimiento(String id, String usuarioId, String cuentaId, Date fecha, double cantidad, int tipo, String concepto, String tipoPago) {
        this.id = id;
        this.usuarioId = usuarioId;
        this.cuentaId = cuentaId;
        this.fecha = fecha;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.concepto = concepto;
        this.tipoPago = tipoPago;
    }

    @Override
    public String toString() {
        return "Movimiento{" +
                "id='" + id + '\'' +
                ", usuarioId='" + usuarioId + '\'' +
                ", cuentaId='" + cuentaId + '\'' +
                ", fecha=" + fecha +
                ", cantidad=" + cantidad +
                ", tipo=" + tipo +
                ", concepto='" + concepto + '\'' +
                ", tipoPago='" + tipoPago + '\'' +
                '}';
    }
}