package mx.ihsa.android.mybank.repository.firebase;

import android.os.SystemClock;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mx.ihsa.android.mybank.model.Sucursal;

public class SucursalRepository {

    private static final String TAG = "*** SucursalRepository ";

    private static FirebaseFirestore firebaseFirestore;


    public static List<Sucursal> getSucursales() {
        List<Sucursal> retVal = null;

        firebaseFirestore = FirebaseFirestore.getInstance();

        Task<QuerySnapshot> task =
                firebaseFirestore.collection(Sucursal.NOMBRE_TABLA).get();

        while (!task.isSuccessful()) {
            SystemClock.sleep(5);
        }

        if (task.getResult().getDocuments().isEmpty()) {
            retVal = Collections.emptyList();
        } else {
            retVal = new ArrayList<>();

            for(DocumentSnapshot doc : task.getResult().getDocuments()) {
                retVal.add(doc.toObject(Sucursal.class));
            }
        }


        return retVal;

    }


    private SucursalRepository(){}
}