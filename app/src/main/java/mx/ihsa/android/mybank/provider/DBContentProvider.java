package mx.ihsa.android.mybank.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import lombok.Builder;
import lombok.Getter;
import mx.ihsa.android.mybank.data.DBHelper;
import mx.ihsa.android.mybank.model.Movimiento;
import mx.ihsa.android.mybank.model.Usuario;

public class DBContentProvider extends ContentProvider {

    private static UriMatcher uriMatcher;

    private static final int ALL_ELEMENTS_USUARIO = 0;
    private static final int SINGLE_ELEMENT_USUARIO = 1;

    private static final int ALL_ELEMENTS_MOVIMIENTO = 2;
    private static final int SINGLE_ELEMENT_MOVIMIENTO = 3;

    private DBHelper dbHelper;


    // para resolver las invocaciones al content provider
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        // El URI va a recibir un valor numérico
        uriMatcher.addURI(ProviderTags.AUTHORITY, Usuario.NOMBRE_TABLA + "/*", SINGLE_ELEMENT_USUARIO);
        uriMatcher.addURI(ProviderTags.AUTHORITY, Usuario.NOMBRE_TABLA, ALL_ELEMENTS_USUARIO);

        uriMatcher.addURI(ProviderTags.AUTHORITY, Movimiento.NOMBRE_TABLA + "/#", SINGLE_ELEMENT_MOVIMIENTO);
        uriMatcher.addURI(ProviderTags.AUTHORITY, Movimiento.NOMBRE_TABLA, ALL_ELEMENTS_MOVIMIENTO);
    }


    public DBContentProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rows = 0;

        int codigo = uriMatcher.match(uri);
        ProviderQueryHelper pqh;
        switch (codigo) {
            case ALL_ELEMENTS_USUARIO:
                pqh =
                        ProviderQueryHelper.builder()
                                .table(Usuario.NOMBRE_TABLA)
                                .values(values)
                                .build();

                rows = insertIntoDb(pqh);
                break;

            case SINGLE_ELEMENT_USUARIO:
                break;

            case ALL_ELEMENTS_MOVIMIENTO:
                pqh =
                        ProviderQueryHelper.builder()
                                .table(Movimiento.NOMBRE_TABLA)
                                .values(values)
                                .build();

                rows = insertIntoDb(pqh);
                break;

            case SINGLE_ELEMENT_MOVIMIENTO:
                break;

            default:
                Log.w(ProviderTags.TAG, "Código inválido: " + codigo);
        }

        return ContentUris.withAppendedId(uri, rows);
    }

    private long insertIntoDb(ProviderQueryHelper pqh) {
        long rows = 0;

        try (SQLiteDatabase db = dbHelper.getWritableDatabase()) {
            rows = db.insert(pqh.getTable(), null, pqh.getValues());
        } catch (Exception e) {
            Log.w(ProviderTags.TAG, e);
        }

        return rows;
    }


    private int update(ProviderQueryHelper pqh) {
        int retVal = 0;

        try (SQLiteDatabase db = dbHelper.getWritableDatabase()) {

            retVal =
                    db.update(
                            pqh.getTable(),
                            pqh.getValues(),
                            pqh.getSelection(),
                            pqh.getSelectionArgs()
                    );

        } catch (Exception e) {
            Log.w(ProviderTags.TAG, e);
        }

        return retVal;
    }

    @Override
    public boolean onCreate() {
        dbHelper = new DBHelper(getContext());
        return dbHelper != null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {


        Cursor retVal = null;

        try {
            ProviderQueryHelper qh = null;

            int codigo = uriMatcher.match(uri);
            switch (codigo) {
                case ALL_ELEMENTS_USUARIO:
                    qh =
                            ProviderQueryHelper.builder()
                                    .uri(uri)
                                    .table(Usuario.NOMBRE_TABLA)
                                    .sortOrder(Usuario.NOMBRE)
                                    .build();
                    retVal = getDataFromTable(qh);
                    break;

                case SINGLE_ELEMENT_USUARIO:
                    selection = Usuario.CORREO + " = ?";
                    selectionArgs = new String[]{uri.getLastPathSegment()};

                    qh =
                            ProviderQueryHelper.builder()
                                    .uri(uri)
                                    .table(Usuario.NOMBRE_TABLA)
                                    .columns(projection)
                                    .selection(selection)
                                    .selectionArgs(selectionArgs)
                                    .build();

                    retVal = getDataFromTable(qh);
                    break;

                case ALL_ELEMENTS_MOVIMIENTO:
                    qh =
                            ProviderQueryHelper.builder()
                                    .uri(uri)
                                    .table(Movimiento.NOMBRE_TABLA)
                                    .columns(projection)
                                    .selection(selection)
                                    .selectionArgs(selectionArgs)
                                    .sortOrder(sortOrder)
                                    .groupBy(Movimiento.TIPO)
                                    .build();
                    retVal = getDataFromTable(qh);
                    break;

                case SINGLE_ELEMENT_MOVIMIENTO:
                    break;

                default:
                    Log.w(ProviderTags.TAG, "Código inválido: " + codigo);
            }
        } catch (Exception e) {
            Log.w(ProviderTags.TAG, e);
        }

        return retVal;
    }


    private Cursor getDataFromTable(ProviderQueryHelper pqh) {

        Cursor retVal = null;

        try {
            SQLiteDatabase db = dbHelper.getReadableDatabase();

            retVal =
                    db.query(
                            pqh.getTable(),
                            pqh.getColumns(),
                            pqh.getSelection(),
                            pqh.getSelectionArgs(),
                            pqh.getGroupBy(),
                            pqh.getHaving(),
                            pqh.getSortOrder()
                    );


        } catch (Exception e) {
            Log.w(ProviderTags.TAG, e);
        }

        return retVal;
    }


    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

        ProviderQueryHelper pqh;

        if (uriMatcher.match(uri) == ALL_ELEMENTS_USUARIO) {
            pqh =
                    ProviderQueryHelper.builder()
                            .table(Usuario.NOMBRE_TABLA)
                            .values(values)
                            .selection(selection)
                            .selectionArgs(selectionArgs)
                            .build();
        } else {
            pqh =
                    ProviderQueryHelper.builder()
                            .table(Movimiento.NOMBRE_TABLA)
                            .values(values)
                            .selection(selection)
                            .selectionArgs(selectionArgs)
                            .build();

        }

        return update(pqh);
    }
}
