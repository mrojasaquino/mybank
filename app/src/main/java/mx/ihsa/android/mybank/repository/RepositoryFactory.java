package mx.ihsa.android.mybank.repository;


import android.util.Log;

import mx.ihsa.android.mybank.Flags;


public class RepositoryFactory {

    private static final String TAG = "*** RepositoryFactory ";

    public static UsuarioRepository getUsuarioRepository() {
        UsuarioRepository instRepo = null;

        try {
            Class<UsuarioRepository> repo =
                    (Class<UsuarioRepository>) Class.forName(
                            "mx.ihsa.android.mybank.repository." + getStorageEngineType()
                                    + ".UsuarioRepository"
                    );
            instRepo = repo.newInstance();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }

        return instRepo;
    }

    public static MovimientoRepository getMovimientoRepository() {
        MovimientoRepository instRepo = null;

        try {
            Class<MovimientoRepository> repo =
                    (Class<MovimientoRepository>) Class.forName(
                            "mx.ihsa.android.mybank.repository." + getStorageEngineType()
                                    + ".MovimientoRepository"
                    );
            instRepo = repo.newInstance();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }

        return instRepo;
    }

    public static CuentaRepository getCuentaRepository() {
        CuentaRepository instRepo = null;

        try {
            Class<CuentaRepository> repo =
                    (Class<CuentaRepository>) Class.forName(
                            "mx.ihsa.android.mybank.repository." + getStorageEngineType()
                                    + ".CuentaRepository"
                    );
            instRepo = repo.newInstance();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }

        return instRepo;
    }



    private static String getStorageEngineType() {
        String retVal = null;

        switch (Flags.STORAGE) {
            case Flags.USE_CONTENT_PROVIDER:
                retVal = "contprov";
                break;

            case Flags.USE_DATABASE:
                retVal = "sqlite";
                break;

            case Flags.USE_FIREBASE:
                retVal = "firebase";
                break;

            default:
                retVal = "unknown";
        }

        return retVal;
    }
}
