package mx.ihsa.android.mybank.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DB_NAME = "my_bank.db";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(UtilsDB.CREATE_TABLE_USUARIO);
        db.execSQL(UtilsDB.CREATE_TABLE_MOVIMIENTO);
        db.execSQL(UtilsDB.CREATE_TABLE_CUENTA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(UtilsDB.DROP_TABLE_CUENTA);
        db.execSQL(UtilsDB.DROP_TABLE_MOVIMIENTO);
        db.execSQL(UtilsDB.DROP_TABLE_USUARIO);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
