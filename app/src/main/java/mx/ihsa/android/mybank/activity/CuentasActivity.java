package mx.ihsa.android.mybank.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.UUID;

import mx.ihsa.android.mybank.R;
import mx.ihsa.android.mybank.model.Cuenta;
import mx.ihsa.android.mybank.model.Usuario;
import mx.ihsa.android.mybank.repository.CuentaRepository;
import mx.ihsa.android.mybank.repository.RepositoryFactory;
import mx.ihsa.android.mybank.repository.UsuarioRepository;

public class CuentasActivity extends AppCompatActivity {

    private Usuario usuario;
    private Context ctx;
    private Bundle bundle;
    private TextView tvUsuarioNombre;
    private EditText etNoCuenta;
    private EditText etCuentaSaldo;
    private Button btnCtaAdelante;
    private Button btnCtaAtras;
    private Button btnCtaNueva;
    private Button btnCtaGuardar;
    private Button btnCtaCancelar;
    private ProgressBar pbGuardarCuenta;

    private CuentaRepository ctaRepo;


    private int posActual = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuentas);

        bind();
        loadData();
    }


    private void bind() {
        ctx = getApplicationContext();

        bundle = this.getIntent().getExtras();

        usuario = (Usuario) bundle.get(Usuario.NOMBRE_TABLA);

        tvUsuarioNombre = findViewById(R.id.tv_usuario_nombre);

        etNoCuenta = findViewById(R.id.et_no_cuenta);
        etNoCuenta.setEnabled(false);

        etCuentaSaldo = findViewById(R.id.et_cuenta_saldo);
        etCuentaSaldo.setEnabled(false);

        btnCtaAtras = findViewById(R.id.btn_cta_atras);
        btnCtaAtras.setEnabled(false);
        btnCtaAtras.setOnClickListener(t -> retroceder());

        btnCtaAdelante = findViewById(R.id.btn_cta_adelante);
        btnCtaAdelante.setEnabled(false);
        btnCtaAdelante.setOnClickListener(t -> avanzar());


        btnCtaNueva = findViewById(R.id.btn_cta_nueva);
        btnCtaNueva.setEnabled(true);
        btnCtaNueva.setOnClickListener(t -> agregar());

        btnCtaGuardar = findViewById(R.id.btn_cta_guardar);
        btnCtaGuardar.setEnabled(false);
        btnCtaGuardar.setOnClickListener(t -> guardar());

        btnCtaCancelar = findViewById(R.id.btn_cta_cancelar);
        btnCtaCancelar.setEnabled(false);
        btnCtaCancelar.setOnClickListener(t -> cancelar());


        pbGuardarCuenta = findViewById(R.id.pb_guardar_cuenta);
        pbGuardarCuenta.setVisibility(View.GONE);

    }

    private void loadData() {
        tvUsuarioNombre.setText(usuario.getNombre());

        ctaRepo = RepositoryFactory.getCuentaRepository();

        usuario.setCuentas(ctaRepo.getCuentasForUsuario(ctx, usuario.getId()));

        if (!usuario.getCuentas().isEmpty()) {
            actualizaCampos(usuario.getCuentas().get(0));

            if (usuario.getCuentas().size() > 1) {
                btnCtaAdelante.setEnabled(true);
            }
        }
    }

    private void retroceder() {
        navegar(-1);
    }

    private void navegar(int pos) {

        switch (pos) {
            case -1:
                if (posActual > 0) {
                    posActual--;
                }
                break;

            case 1:
                if (posActual < usuario.getCuentas().size() - 1) {
                    posActual++;
                }
                break;
            default:
                posActual = pos;
        }

        if (posActual == 0 && usuario.getCuentas().size() > 0) {
            btnCtaAtras.setEnabled(false);
            btnCtaAdelante.setEnabled(true);
        } else if (posActual > 0 && posActual < usuario.getCuentas().size() - 1) {
            btnCtaAtras.setEnabled(true);
            btnCtaAdelante.setEnabled(true);
        } else if (posActual == usuario.getCuentas().size() - 1) {
            btnCtaAtras.setEnabled(true);
            btnCtaAdelante.setEnabled(false);
        }

        actualizaCampos(usuario.getCuentas().get(posActual));
    }


    private void agregar() {
        cambiarEstatus(true);
        etNoCuenta.setText(null);
        etCuentaSaldo.setText("0");
        btnCtaAtras.setEnabled(false);
        btnCtaAdelante.setEnabled(false);
        btnCtaNueva.setEnabled(false);
        btnCtaGuardar.setEnabled(true);
        btnCtaCancelar.setEnabled(true);
    }

    private void guardar() {
        if (TextUtils.isEmpty(etCuentaSaldo.getText())
                || TextUtils.isEmpty(etNoCuenta.getText())) {
            mostrarToast("Debe proporcionar número de cuenta y saldo.");
        } else {
            new DataSaver().execute();
        }


    }

    private void avanzar() {
        navegar(+1);
    }

    private void cancelar() {
        cambiarEstatus(true);
        navegar(0);
        btnCtaNueva.setEnabled(true);
        btnCtaGuardar.setEnabled(false);
        btnCtaCancelar.setEnabled(false);
    }

    private void actualizaCampos(Cuenta cuenta) {
        etNoCuenta.setText(cuenta.getNumero());
        etCuentaSaldo.setText(String.valueOf(cuenta.getSaldo()));
    }


    private void cambiarEstatus(boolean estatus) {
        etCuentaSaldo.setEnabled(estatus);
        etNoCuenta.setEnabled(estatus);
    }

    @Override
    protected void onStop() {
        super.onStop();
        finishAndRemoveTask();
    }

    private void mostrarToast(String mensaje) {
        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
    }


    private class DataSaver extends AsyncTask<Void, Void, Void> {
        private boolean registrado;

        @Override
        protected void onPreExecute() {
            pbGuardarCuenta.setIndeterminate(true);
            pbGuardarCuenta.setVisibility(View.VISIBLE);

            btnCtaNueva.setEnabled(false);
            btnCtaGuardar.setEnabled(false);
            btnCtaCancelar.setEnabled(false);

            cambiarEstatus(false);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            UsuarioRepository repo = RepositoryFactory.getUsuarioRepository();

            Cuenta cuenta = Cuenta.builder()
                    .id(UUID.randomUUID().toString())
                    .usuarioId(usuario.getId())
                    .numero(etNoCuenta.getText().toString())
                    .saldo(Double.valueOf(etCuentaSaldo.getText().toString()))
                    .build();

            usuario.getCuentas().add(cuenta);

            registrado = repo.agregarCuenta(ctx, cuenta);

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            pbGuardarCuenta.setIndeterminate(false);
            pbGuardarCuenta.setVisibility(View.GONE);

            if (registrado) {
                mostrarToast("La cuenta se agregó exitosamente");
            } else {
                mostrarToast("No fue posible agregar la cuenta.");
                cambiarEstatus(false);
            }

            usuario.setCuentas(ctaRepo.getCuentasForUsuario(ctx, usuario.getId()));

            navegar(0);
            btnCtaNueva.setEnabled(true);
            btnCtaGuardar.setEnabled(false);
            btnCtaCancelar.setEnabled(false);
        }
    }
}
