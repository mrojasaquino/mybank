package mx.ihsa.android.mybank.repository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.util.UUID;

import mx.ihsa.android.mybank.data.DBHelper;
import mx.ihsa.android.mybank.model.Cuenta;
import mx.ihsa.android.mybank.model.Usuario;

public class UsuarioRepository extends mx.ihsa.android.mybank.repository.UsuarioRepository {

    private static final String TAG = "*** UsuarioRepository ";

    @Override
    public Usuario recuperar(Context ctx, String correo, String clave) {
        Usuario retVal = null;

        DBHelper dbHelper = new DBHelper(ctx);

        Log.i(TAG, "Usuario :" + correo + " / " + clave);

        try (
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                Cursor cursor =
                        db.query(
                                Usuario.NOMBRE_TABLA,
                                null,
                                Usuario.CORREO + " = ? AND " + Usuario.CLAVE + " = ?",
                                new String[]{correo, clave},
                                null,
                                null,
                                null
                        )
        ) {
            if (cursor.moveToFirst()) {
                Log.i(TAG, "Trajimos un usuario");
                retVal =
                        Usuario.builder()
                                .id(cursor.getString(0))
                                .nombre(cursor.getString(1))
                                .clave(cursor.getString(2))
                                .direccion(cursor.getString(3))
                                .correo(cursor.getString(4))
                                .build();
            } else {
                Log.i(TAG, "No tenemos usuarios");
            }

        } catch (SQLiteException e) {
            Log.w(TAG, e);
        }

        return retVal;
    }


    @Override
    public boolean guardar(Context ctx, Usuario usuario) {

        boolean retVal = false;

        if (usuario == null) {
            throw new IllegalStateException("El usuario nu puede ser nulo");
        } else {
            DBHelper dbHelper = new DBHelper(ctx);
            ContentValues values = new ContentValues();


            try (SQLiteDatabase db = dbHelper.getWritableDatabase()) {
                long num = 0;

                if (usuario.getId() == null || usuario.getId().isEmpty()) {

                    usuario.setId(UUID.randomUUID().toString());

                    Cuenta cuenta1 = new Cuenta().builder()
                            .id(UUID.randomUUID().toString())
                            .usuarioId(usuario.getId())
                            .numero("Cuenta 1 - " + usuario.getCorreo())
                            .saldo(1000)
                            .build();

                    Cuenta cuenta2 = new Cuenta().builder()
                            .id(UUID.randomUUID().toString())
                            .usuarioId(usuario.getId())
                            .numero("Cuenta 2 - " + usuario.getCorreo())
                            .saldo(1000)
                            .build();

                    db.beginTransaction();

                    try {
                        values.put(Usuario.ID, usuario.getId());
                        values.put(Usuario.NOMBRE, usuario.getNombre());
                        values.put(Usuario.CLAVE, usuario.getClave());
                        values.put(Usuario.DIRECCION, usuario.getDireccion());
                        values.put(Usuario.CORREO, usuario.getCorreo());

                        num = db.insert(Usuario.NOMBRE_TABLA, null, values);

                        if(num > 0) {
                            values = new ContentValues();
                            values.put(Cuenta.ID, cuenta1.getId());
                            values.put(Cuenta.USUARIO_ID, cuenta1.getUsuarioId());
                            values.put(Cuenta.NUMERO, cuenta1.getNumero());
                            values.put(Cuenta.SALDO, cuenta1.getSaldo());

                            num = db.insert(Cuenta.NOMBRE_TABLA, null, values);

                            values = new ContentValues();
                            values.put(Cuenta.ID, cuenta2.getId());
                            values.put(Cuenta.USUARIO_ID, cuenta2.getUsuarioId());
                            values.put(Cuenta.NUMERO, cuenta2.getNumero());
                            values.put(Cuenta.SALDO, cuenta2.getSaldo());

                            num = db.insert(Cuenta.NOMBRE_TABLA, null, values);
                        }


                        db.setTransactionSuccessful();

                        Log.i(TAG, "Se insertaron " + num + " usuarios.");
                    } catch (SQLiteException e) {
                        Log.w(TAG, e);
                    } finally {
                        db.endTransaction();
                    }
                } else {
                    num =
                            db.update(
                                    Usuario.NOMBRE_TABLA,
                                    values,
                                    Usuario.ID + "= ?",
                                    new String[]{usuario.getId()}
                            );

                    Log.i(TAG, "Se actualizaron " + num + " usuarios.");
                }

                retVal = num > 0;
            } catch (SQLiteException e) {
                Log.w(TAG, e);
            }
        }

        return retVal;
    }


    @Override
    public boolean agregarCuenta(Context ctx, Cuenta cuenta) {
        long num = 0;

        DBHelper dbHelper = new DBHelper(ctx);

        ContentValues values = new ContentValues();
        values.put(Cuenta.ID, cuenta.getId());
        values.put(Cuenta.USUARIO_ID, cuenta.getUsuarioId());
        values.put(Cuenta.NUMERO, cuenta.getNumero());
        values.put(Cuenta.SALDO, cuenta.getSaldo());

        try(SQLiteDatabase db = dbHelper.getWritableDatabase()) {
            num = db.insert(Cuenta.NOMBRE_TABLA, null, values);
        } catch (SQLiteException e) {
            Log.w(TAG, e);
        }


        return num > 0;
    }
}
