package mx.ihsa.android.mybank.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import mx.ihsa.android.mybank.R;
import mx.ihsa.android.mybank.model.Movimiento;
import mx.ihsa.android.mybank.model.TipoMovimiento;
import mx.ihsa.android.mybank.model.Usuario;
import mx.ihsa.android.mybank.repository.CuentaRepository;
import mx.ihsa.android.mybank.repository.MovimientoRepository;
import mx.ihsa.android.mybank.repository.RepositoryFactory;

public class TransferenciaActivity extends AppCompatActivity {

    private Spinner spnCuentaOrigen;
    private Spinner spnCuentaDestino;
    private EditText etMontoTransfer;
    private EditText etConceptoTransfer;
    private Button btnGuardarMontoT;
    private ProgressBar pbGuardaPagoT;

    private Bundle bundle;
    private Context ctx;

    private Usuario usuario;

    private String cuentaOrigen;
    private String cuentaDestino;

    private CuentaRepository ctaRepo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transferencia);

        init();
        loadData();
    }


    private void init() {
        spnCuentaOrigen = findViewById(R.id.spn_cuenta_origen);
        spnCuentaOrigen.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        cuentaOrigen = usuario.getCuentas().get(spnCuentaOrigen.getSelectedItemPosition()).getId();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );

        spnCuentaDestino = findViewById(R.id.spn_cuenta_destino);
        spnCuentaDestino.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        cuentaDestino = usuario.getCuentas().get(spnCuentaDestino.getSelectedItemPosition()).getId();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );

        etMontoTransfer = findViewById(R.id.et_monto_transfer);
        etConceptoTransfer = findViewById(R.id.et_concepto_transfer);


        btnGuardarMontoT = findViewById(R.id.btn_guardar_monto_t);
        btnGuardarMontoT.setOnClickListener(l -> guardarTransferencia());


        pbGuardaPagoT = findViewById(R.id.pb_guarda_pago_t);
        pbGuardaPagoT.setVisibility(View.GONE);

        ctaRepo = RepositoryFactory.getCuentaRepository();
    }


    private void loadData() {
        bundle = this.getIntent().getExtras();
        usuario = (Usuario) bundle.get(Usuario.NOMBRE_TABLA);
        ctx = getApplicationContext();

        usuario.setCuentas(ctaRepo.getCuentasForUsuario(ctx, usuario.getId()));

        if (usuario.getCuentas().isEmpty()) {
            mostrarToast("El usuario no tiene cuentas.");
        } else {
            AtomicReference<List<String>> elementos = new AtomicReference<>(new ArrayList<>());
            usuario.getCuentas().forEach(c -> {
                elementos.get().add(c.getNumero());
            });

            if (elementos.get() != null) {
                if(elementos.get().isEmpty() || elementos.get().size() < 2) {
                    mostrarToast("Debe tener al menos 2 cuentas registradas.");
                } else {
                    ArrayAdapter<String> adapterOrigen =
                            new ArrayAdapter<>(ctx, android.R.layout.simple_spinner_item, elementos.get());
                    adapterOrigen.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spnCuentaOrigen.setAdapter(adapterOrigen);


                    ArrayAdapter<String> adapterDestino =
                            new ArrayAdapter<>(ctx, android.R.layout.simple_spinner_item, elementos.get());
                    adapterDestino.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spnCuentaDestino.setAdapter(adapterDestino);

                    cuentaOrigen = usuario.getCuentas().get(0).getId();
                    cuentaDestino = usuario.getCuentas().get(1).getId();

                    spnCuentaOrigen.setSelection(0);
                    spnCuentaDestino.setSelection(1);
                }


            }
        }
    }


    private void cambiarEstatus(boolean estado) {
        spnCuentaOrigen.setEnabled(estado);
        spnCuentaDestino.setEnabled(estado);
        etMontoTransfer.setEnabled(estado);
        etConceptoTransfer.setEnabled(estado);
        btnGuardarMontoT.setEnabled(estado);
    }


    private void mostrarToast(String mensaje) {
        Toast.makeText(ctx, mensaje, Toast.LENGTH_SHORT).show();
    }


    private void limpiar() {
        spnCuentaOrigen.setSelection(0);
        spnCuentaDestino.setSelection(0);

        cuentaOrigen = usuario.getCuentas().get(spnCuentaOrigen.getSelectedItemPosition()).getId();
        cuentaDestino = usuario.getCuentas().get(spnCuentaDestino.getSelectedItemPosition()).getId();

        etConceptoTransfer.setText(null);
        etMontoTransfer.setText(null);
    }


    private void guardarTransferencia() {
        if (TextUtils.isEmpty(etMontoTransfer.getText())
                || TextUtils.isEmpty(etConceptoTransfer.getText())) {
            mostrarToast("Debe introducir el monto y el concepto de la transferencia.");
        } else if (spnCuentaDestino.getSelectedItem() == spnCuentaOrigen.getSelectedItem()) {
            mostrarToast("La cuenta destino debe ser distinta a la cuenta origen.");
        } else if(Double.valueOf(etMontoTransfer.getText().toString()) > 1000){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Alerta por monto")
                    .setMessage("El monto de la transferencia es superior a 1000")
            .setCancelable(true)
            .setNeutralButton("Aceptar", (dialog, which) -> new TransferenciaSaver().execute())
            .show();
        } else {
            new TransferenciaSaver().execute();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        finishAndRemoveTask();
    }


    private class TransferenciaSaver extends AsyncTask<Void, Void, Void> {

        private boolean registrado;

        @Override
        protected void onPreExecute() {
            pbGuardaPagoT.setIndeterminate(true);
            pbGuardaPagoT.setVisibility(View.VISIBLE);

            cambiarEstatus(false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            MovimientoRepository repo = RepositoryFactory.getMovimientoRepository();

            Movimiento origen =
                    Movimiento.builder()
                            .cuentaId(cuentaOrigen)
                            .usuarioId(usuario.getId())
                            .cantidad(Double.valueOf(etMontoTransfer.getText().toString()) * -1)
                            .concepto(etConceptoTransfer.getText().toString() + " -> egreso")
                            .tipo(TipoMovimiento.TRANSFERENCIA.getId())
                            .tipoPago("transferencia ->")
                            .fecha(new Date(System.currentTimeMillis()))
                            .build();


                Movimiento destino =
                        Movimiento.builder()
                                .cuentaId(cuentaDestino)
                                .usuarioId(usuario.getId())
                                .cantidad(Double.valueOf(etMontoTransfer.getText().toString()))
                                .concepto(etConceptoTransfer.getText().toString()  + " <- ingreso")
                                .tipo(TipoMovimiento.TRANSFERENCIA.getId())
                                .tipoPago("transferencia <-")
                                .fecha(new Date(System.currentTimeMillis()))
                                .build();


            registrado = repo.transferirMonto(ctx, origen, destino);

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            pbGuardaPagoT.setIndeterminate(false);
            pbGuardaPagoT.setVisibility(View.GONE);

            cambiarEstatus(true);

            if (registrado) {
                mostrarToast("La transferencia fue registrada exitosamente.");
                limpiar();
            } else {
                mostrarToast("La transferencia no fue registrada.");
            }

            finish();
        }
    }
}
