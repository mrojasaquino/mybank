package mx.ihsa.android.mybank.repository.contprov;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import mx.ihsa.android.mybank.model.Movimiento;
import mx.ihsa.android.mybank.model.TipoMovimiento;
import mx.ihsa.android.mybank.provider.ProviderTags;

public class MovimientoRepository extends mx.ihsa.android.mybank.repository.MovimientoRepository {

    private static final String TAG = "*** MovimientoRepository ";

    @Override
    public Map<TipoMovimiento, Double> getTotalesParaUsuario(Context ctx, String usuarioId) {
        Map<TipoMovimiento, Double> retVal = null;
        Uri uri = ProviderTags.URI_CONTENT_MOVIMIENTO;

        final String[] proyeccion = new String[]{Movimiento.TIPO, "sum(cantidad)", "count(*)"};
        final String seleccion = Movimiento.USUARIO_ID + " = ?";
        final String[] argumentos = new String[]{usuarioId};

        try (Cursor cursor =
                     ctx.getContentResolver().query(
                             uri,
                             proyeccion,
                             seleccion,
                             argumentos,
                             Movimiento.FECHA)
        ) {

            if (cursor == null) {
                Log.w(ProviderTags.TAG, "No se encontraron movimientos.");
            } else {
                retVal = new HashMap<>();
                while (cursor.moveToNext()) {
                    Log.i(TAG, "Usuario : " + usuarioId + ", tipo : " + cursor.getInt(0) + ", registros : " + cursor.getDouble(2));
                    retVal.put(TipoMovimiento.valueOf(cursor.getInt(0)), cursor.getDouble(1));
                }
            }
        } catch (Exception e) {
            Log.w(ProviderTags.TAG, e);
        }

        if (retVal == null) {
            retVal = Collections.emptyMap();
        }

        return retVal;
    }

    @Override
    public boolean guardarMovimiento(Context ctx, Movimiento movimiento) {
        boolean retVal = false;


        return retVal;
    }

    @Override
    public boolean transferirMonto(Context ctx, Movimiento origen, Movimiento destino) {
        boolean retVal = false;


        return retVal;
    }
}
