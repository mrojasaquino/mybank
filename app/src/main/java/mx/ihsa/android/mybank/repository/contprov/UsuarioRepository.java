package mx.ihsa.android.mybank.repository.contprov;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import mx.ihsa.android.mybank.model.Cuenta;
import mx.ihsa.android.mybank.model.Usuario;
import mx.ihsa.android.mybank.provider.ProviderTags;

public class UsuarioRepository extends mx.ihsa.android.mybank.repository.UsuarioRepository {

    private static final String TAG = "*** UsuarioRepository ";


    @Override
    public Usuario recuperar(Context ctx, String correo, String clave) {
        Uri uri = Uri.parse(ProviderTags.URI_STRING_USUARIO + '/' + correo);
        Usuario retVal = null;

        try (Cursor cursor =
                     ctx.getContentResolver().query(
                             uri,
                             null,
                             null,
                             new String[]{correo}
                             , null)
        ) {
            if (cursor != null && cursor.moveToNext()) {
                if (cursor.getString(2) != null
                        && cursor.getString(2).equals(clave)) {
                    retVal =
                            Usuario.builder()
                                    .id(String.valueOf(cursor.getLong(0)))
                                    .nombre(cursor.getString(1))
                                    .clave(cursor.getString(2))
                                    .direccion(cursor.getString(3))
                                    .correo(cursor.getString(4))
                                    .build();
                } else {
                    Log.w(TAG, "La contraseña no corresponde con la del usuario : " + correo);
                }
            } else {
                Log.w(TAG, "No se encontró al usuario con el correo :" + correo);
            }
        } catch (Exception e) {
            Log.w(TAG, e);
        }

        return retVal;
    }


    @Override
    public boolean guardar(Context ctx, Usuario usuario) {
        boolean retVal;
        Uri uri = ProviderTags.URI_CONTENT_USUARIO;

        if (usuario == null) {
            throw new IllegalStateException("El usuario nu puede ser nulo");
        } else {
            ContentValues values = new ContentValues();
            values.put(Usuario.NOMBRE, usuario.getNombre());
            values.put(Usuario.CLAVE, usuario.getClave());
            values.put(Usuario.DIRECCION, usuario.getDireccion());
            values.put(Usuario.CORREO, usuario.getCorreo());

            if (usuario.getId() == null || usuario.getId().isEmpty()) {
                Uri resultado = ctx.getContentResolver().insert(uri, values);

                retVal = Integer.valueOf(resultado.getLastPathSegment()) > 0;

            } else {
                retVal =
                        ctx.getContentResolver().update(
                                uri,
                                values,
                                Usuario.NOMBRE_TABLA + " = ? ",
                                new String[]{usuario.getId()}
                        ) > 0;
            }
        }

        return retVal;
    }


    @Override
    public boolean agregarCuenta(Context ctx, Cuenta cuenta) {
        return false;
    }

}
