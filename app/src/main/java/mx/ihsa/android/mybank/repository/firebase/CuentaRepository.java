package mx.ihsa.android.mybank.repository.firebase;

import android.content.Context;
import android.os.SystemClock;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import mx.ihsa.android.mybank.model.Cuenta;

public class CuentaRepository extends mx.ihsa.android.mybank.repository.CuentaRepository {

    private static final String TAG = "*** CuentaRepository ";

    private FirebaseFirestore firebaseFirestore;


    @Override
    public List<Cuenta> getCuentasForUsuario(Context ctx, String usuarioId) {
        List<Cuenta> retVal = null;

        firebaseFirestore = FirebaseFirestore.getInstance();

        Task<QuerySnapshot> task =
                firebaseFirestore.collection(Cuenta.NOMBRE_TABLA)
                        .whereEqualTo(Cuenta.USUARIO_ID, usuarioId)
                        .orderBy(Cuenta.NUMERO)
                        .get();

        while (!task.isSuccessful()) {
            SystemClock.sleep(5);
        }

        if (!task.getResult().getDocuments().isEmpty()) {
            retVal =
                    task.getResult().getDocuments().stream()
                            .map(t -> t.toObject(Cuenta.class))
                            .collect(Collectors.toList());
        }


        if(retVal == null) {
            retVal = Collections.emptyList();
        }

        return retVal;
    }
}
