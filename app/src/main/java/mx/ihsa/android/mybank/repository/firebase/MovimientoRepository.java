package mx.ihsa.android.mybank.repository.firebase;

import android.content.Context;
import android.os.SystemClock;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import mx.ihsa.android.mybank.model.Cuenta;
import mx.ihsa.android.mybank.model.Movimiento;
import mx.ihsa.android.mybank.model.TipoMovimiento;

public class MovimientoRepository extends mx.ihsa.android.mybank.repository.MovimientoRepository {

    private static final String TAG = "*** MovimientoRepository ";

    private FirebaseFirestore firebaseFirestore;


    @Override
    public Map<TipoMovimiento, Double> getTotalesParaUsuario(Context ctx, String usuarioId) {
        AtomicReference<Map<TipoMovimiento, Double>> retVal =
                new AtomicReference<>(new HashMap<>());

        firebaseFirestore = FirebaseFirestore.getInstance();

        Task<QuerySnapshot> task =
                firebaseFirestore.collection(Movimiento.NOMBRE_TABLA)
                        .whereEqualTo("usuarioId", usuarioId)
                        .get();

        while (!task.isSuccessful()) {
            SystemClock.sleep(5);
        }

        if (!task.getResult().getDocuments().isEmpty()) {
            Log.w(TAG, "Documentos : " + task.getResult().getDocuments().size());

            List<Movimiento> movs = new ArrayList<>();
            for (DocumentSnapshot doc : task.getResult().getDocuments()) {
                movs.add(doc.toObject(Movimiento.class));
            }

            movs.stream()
                    .collect(Collectors.groupingBy(Movimiento::getTipo))
                    .forEach((tipo, movtos) -> {

                        Log.w(TAG, "Tipo : " + tipo + ", movs : " + movtos.size());

                        retVal.get().put(TipoMovimiento.valueOf(tipo), (double) movtos.stream().count());

                        Log.d(TAG, "Suma: " + retVal.get().get(TipoMovimiento.valueOf(tipo)));
                    });

            retVal.get()
                    .forEach((tipo, monto) -> Log.w(TAG, "Tipo: " + tipo + ", Monto : " + monto));
        }


        return retVal.get();
    }


    @Override
    public boolean guardarMovimiento(Context ctx, Movimiento movimiento) {
        AtomicReference<Boolean> guardado = new AtomicReference<>(Boolean.FALSE);

        movimiento.setId(UUID.randomUUID().toString());

        firebaseFirestore = FirebaseFirestore.getInstance();

        DocumentReference movto =
                firebaseFirestore
                        .collection(Movimiento.NOMBRE_TABLA)
                        .document(movimiento.getId());

        DocumentReference cta =
                firebaseFirestore
                        .collection(Cuenta.NOMBRE_TABLA)
                        .document(movimiento.getCuentaId());


        Log.i(TAG, movto.toString());
        Log.i(TAG, cta.toString());


        Task task =
                firebaseFirestore.runTransaction(t -> {

                    DocumentSnapshot docCta = t.get(cta);
                    double saldo = docCta.getDouble(Cuenta.SALDO) + movimiento.getCantidad();

                    t.set(movto, movimiento);
                    t.update(cta, Cuenta.SALDO, saldo);

                    return null;
                })
                        .addOnSuccessListener(t -> guardado.set(Boolean.TRUE));


        while (!task.isSuccessful()) {
            SystemClock.sleep(5);
        }


        return guardado.get();
    }

    /**
     * Se registran dos movimientos: el de egreso de la cuenta origen y el de ingreso a la cuenta
     * destino. El primero se registra como negativo y el segundo como positivo. Se afectan los saldos
     * de ambas cuentas.
     * @param ctx El contexto de la aplicación.
     * @param origen El movimiento origen
     * @param destino El movimiento destino
     * @return Si fue posible o no registrar los movimientos.
     */
    @Override
    public boolean transferirMonto(Context ctx, Movimiento origen, Movimiento destino) {
        AtomicReference<Boolean> guardado = new AtomicReference<>(Boolean.FALSE);

        firebaseFirestore = FirebaseFirestore.getInstance();

        origen.setId(UUID.randomUUID().toString());
        destino.setId(UUID.randomUUID().toString());

        DocumentReference movOrigen =
                firebaseFirestore
                        .collection(Movimiento.NOMBRE_TABLA)
                        .document(origen.getId());

        DocumentReference movDestino =
                firebaseFirestore
                        .collection(Movimiento.NOMBRE_TABLA)
                        .document(destino.getId());

        DocumentReference saldoOrigen =
                firebaseFirestore
                        .collection(Cuenta.NOMBRE_TABLA)
                        .document(origen.getCuentaId());

        DocumentReference saldoDestino =
                firebaseFirestore
                        .collection(Cuenta.NOMBRE_TABLA)
                        .document(destino.getCuentaId());

        Task task =
                firebaseFirestore.runTransaction(t -> {
                    DocumentSnapshot docCtaOrigen = t.get(saldoOrigen);
                    double sdoOrigen = docCtaOrigen.getDouble(Cuenta.SALDO) + origen.getCantidad();

                    DocumentSnapshot docCtaDest = t.get(saldoDestino);
                    double sdoDestino = docCtaDest.getDouble(Cuenta.SALDO) + destino.getCantidad();

                    t.set(movOrigen, origen);
                    t.set(movDestino, destino);
                    t.update(saldoOrigen, Cuenta.SALDO, sdoOrigen);
                    t.update(saldoDestino, Cuenta.SALDO, sdoDestino);


                    return null;
                }).addOnSuccessListener(l -> guardado.set(Boolean.TRUE));


        while (!task.isSuccessful()) {
            SystemClock.sleep(5);
        }


        return guardado.get();
    }
}
