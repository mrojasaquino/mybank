package mx.ihsa.android.mybank.model;

import java.io.Serializable;
import java.util.List;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Usuario implements Serializable {

    public static final String NOMBRE_TABLA = "usuario";

    public static final String ID = "id";
    public static final String NOMBRE = "nombre";
    public static final String CLAVE = "clave";
    public static final String DIRECCION = "direccion";
    public static final String CORREO = "correo";


    private String id;
    private String nombre;
    private String clave;
    private String direccion;
    private String correo;

    private List<Cuenta> cuentas;


    public Usuario() {}

    public Usuario(String id, String nombre, String clave, String direccion, String correo, List<Cuenta> cuentas) {
        this.id = id;
        this.nombre = nombre;
        this.clave = clave;
        this.direccion = direccion;
        this.correo = correo;
        this.cuentas = cuentas;
    }
}
