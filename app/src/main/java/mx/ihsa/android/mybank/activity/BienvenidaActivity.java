package mx.ihsa.android.mybank.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.Toast;

import mx.ihsa.android.mybank.R;
import mx.ihsa.android.mybank.model.Usuario;

public class BienvenidaActivity extends AppCompatActivity {

    private Context ctx;
    private Bundle bundle;
    private Usuario usuario;

    private Button btnCuentas;
    private Button btnGrafica;
    private Button btnPago;
    private Button btnTransferencia;
    private Button btnAcerca;
    private Button btnSucursales;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bienvenida);

        bind();
        loadData();
    }

    private void bind() {

        ctx = getApplicationContext();

        btnCuentas = findViewById(R.id.btn_cuentas);
        btnCuentas.setOnClickListener(t -> lanzarPantalla(CuentasActivity.class));

        btnGrafica = findViewById(R.id.btn_grafica);
        btnGrafica.setOnClickListener(t -> lanzarPantalla(GraficarAcivity.class));

        btnPago = findViewById(R.id.btn_pago);
        btnPago.setOnClickListener(t -> lanzarPantalla(PagoActivity.class));

        btnTransferencia = findViewById(R.id.btn_transferencia);
        btnTransferencia.setOnClickListener(t -> lanzarPantalla(TransferenciaActivity.class));

        btnAcerca = findViewById(R.id.btn_acerca);
        btnAcerca.setOnClickListener(t -> lanzarPantalla(AcercaDeActivity.class));

        btnSucursales = findViewById(R.id.btn_sucursales);
        btnSucursales.setOnClickListener(t -> lanzarPantalla(SucursalesActivity.class));
    }

    private void loadData(){
        bundle = this.getIntent().getExtras();
        usuario = (Usuario)bundle.get(Usuario.NOMBRE_TABLA);
    }


    private void lanzarPantalla(Class pantalla) {
        if(pantalla == null) {
            mostrarToast("Debe proporcionar una clase.");
        } else {
            Intent intent = new Intent(ctx, pantalla);
            intent.putExtra(Usuario.NOMBRE_TABLA, usuario);
            startActivity(intent);
        }
    }

    private void mostrarToast(String mensaje) {
        Toast.makeText(ctx, mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
