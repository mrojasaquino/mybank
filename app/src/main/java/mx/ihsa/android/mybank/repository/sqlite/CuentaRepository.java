package mx.ihsa.android.mybank.repository.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.SystemClock;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import mx.ihsa.android.mybank.data.DBHelper;
import mx.ihsa.android.mybank.model.Cuenta;
import mx.ihsa.android.mybank.model.Movimiento;

public class CuentaRepository extends mx.ihsa.android.mybank.repository.CuentaRepository {

    private static final String TAG = "*** CuentaRepository ";


    @Override
    public List<Cuenta> getCuentasForUsuario(Context ctx, String usuarioId) {
        List<Cuenta> retVal = null;

        final DBHelper dbHelper = new DBHelper(ctx);
        final String seleccion = Cuenta.USUARIO_ID + " = ?";
        final String[] arcumentos = {usuarioId};

        try(
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                Cursor cursor =
                        db.query(
                                Cuenta.NOMBRE_TABLA,
                                null,
                                seleccion,
                                arcumentos,
                                null,
                                null,
                                Cuenta.NUMERO
                        )
        ) {

            retVal = new ArrayList<>();
            while (cursor.moveToNext()) {
                Cuenta cuenta = Cuenta.builder()
                        .id(cursor.getString(0))
                        .usuarioId(cursor.getString(1))
                        .numero(cursor.getString(2))
                        .saldo(cursor.getDouble(3))
                        .build();
                retVal.add(cuenta);
            }

        } catch (SQLiteException e) {
            Log.w(TAG, e);
        }


        if(retVal == null) {
            retVal = Collections.emptyList();
        }

        return retVal;
    }
}
