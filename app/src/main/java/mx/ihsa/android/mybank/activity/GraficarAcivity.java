package mx.ihsa.android.mybank.activity;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.ihsa.android.mybank.R;
import mx.ihsa.android.mybank.model.TipoMovimiento;
import mx.ihsa.android.mybank.model.Usuario;
import mx.ihsa.android.mybank.repository.MovimientoRepository;
import mx.ihsa.android.mybank.repository.RepositoryFactory;

public class GraficarAcivity extends AppCompatActivity {

    private BarChart chrtBarras;
    private String[] etiquetas = {"Pagos", "Transf."};

    private Bundle bundle;

    private Map<TipoMovimiento, Double> movimientos;
    private Usuario usuario;

    private boolean inicializado;

    private MovimientoRepository repo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graficar);

        bind();
        loadData();
        init();
    }

    private void loadData() {
        bundle = this.getIntent().getExtras();
        usuario = (Usuario) bundle.get(Usuario.NOMBRE_TABLA);


        if(usuario == null) {
            mostrarToast("No fue posible recuperar al usuario.");
        } else {
            movimientos = repo.getTotalesParaUsuario(getApplicationContext(), usuario.getId());

            if(movimientos == null || movimientos.isEmpty()) {
                mostrarToast("No fue posible recuperar los movimientos del usuario.");
            } else {
                inicializado = true;
            }
        }
    }

    private void bind() {
        repo = RepositoryFactory.getMovimientoRepository();
        chrtBarras = findViewById(R.id.chrtBarras);
    }

    private void init() {



        if(inicializado) {
            Description description = new Description();
            description.setPosition( 0,0);
            description.setText("Movimientos del usuario ");

            chrtBarras.setDescription(description);
            chrtBarras.setData(crearBarras());
            crearEtiquetasEjeX(chrtBarras.getXAxis());
            chrtBarras.animateY(2000);
        }
    }

    private BarData crearBarras() {

        float valCol1 = 0;
        float valCol2 = 0;

        if (movimientos.get(TipoMovimiento.PAGO) != null) {
            valCol1 = movimientos.get(TipoMovimiento.PAGO).floatValue();
        }

        if (movimientos.get(TipoMovimiento.TRANSFERENCIA) != null) {
            valCol2 = movimientos.get(TipoMovimiento.TRANSFERENCIA).floatValue();
        }

        BarEntry col1 = new BarEntry(0, valCol1);
        BarEntry col2 = new BarEntry(1, valCol2);

        List<BarEntry> columnas = new ArrayList<>();
        columnas.add(col1);

        BarDataSet barDataSetPagos = new BarDataSet(columnas, "Pagos");
        barDataSetPagos.setColor(Color.BLUE);

        columnas = new ArrayList<>();
        columnas.add(col2);

        BarDataSet barDataSetTransferencias = new BarDataSet(columnas, "Transferencias");
        barDataSetTransferencias.setColor(Color.RED);

        return new BarData(barDataSetPagos, barDataSetTransferencias);
    }

    private void crearEtiquetasEjeX(XAxis xAxis) {
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelRotationAngle(270);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(etiquetas));
        xAxis.setGranularity(100);
    }


    private void mostrarToast(String mensaje) {
        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onStop() {
        super.onStop();
        finishAndRemoveTask();
    }
}
