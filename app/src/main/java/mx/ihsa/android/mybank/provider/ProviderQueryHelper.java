package mx.ihsa.android.mybank.provider;

import android.content.ContentValues;
import android.net.Uri;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ProviderQueryHelper {

    private Uri uri;
    private String table;
    private String[] columns;
    private String selection;
    private String[] selectionArgs;
    private String groupBy;
    private String having;
    private String sortOrder;
    private ContentValues values;
}
