package mx.ihsa.android.mybank.repository;

import android.content.Context;

import java.util.List;

import mx.ihsa.android.mybank.model.Cuenta;

public abstract class CuentaRepository {

    public abstract List<Cuenta> getCuentasForUsuario(Context ctx, String usuarioId);

}
