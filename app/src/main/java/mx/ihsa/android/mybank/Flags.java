package mx.ihsa.android.mybank;

public class Flags {

    public static final int USE_CONTENT_PROVIDER = 0;
    public static final int USE_DATABASE = 1;
    public static final int USE_FIREBASE = 3;


    public static int STORAGE = Flags.USE_DATABASE;
}
