package mx.ihsa.android.mybank.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import mx.ihsa.android.mybank.R;
import mx.ihsa.android.mybank.model.Sucursal;
import mx.ihsa.android.mybank.repository.firebase.SucursalRepository;

public class SucursalesActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sucursales);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        UiSettings ui = mMap.getUiSettings();
        ui.setZoomControlsEnabled(true);


        List<Sucursal> sucursales = SucursalRepository.getSucursales();

        if(!sucursales.isEmpty()) {
            for(Sucursal sucursal : sucursales) {
                LatLng punto =
                        new LatLng(
                                sucursal.getUbicacion().getLatitude(),
                                sucursal.getUbicacion().getLongitude()
                        );
                mMap.addMarker(new MarkerOptions().position(punto).title(sucursal.getNombre()));
            }

            //FIXME : testing
            Location loc = getCurrentLocation();
            //LatLng ihsa = new LatLng(25.6467925, -100.3275182);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(loc.getLatitude(), loc.getLongitude())));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), 13f));
        }


        // Add a marker in Sydney and move the camera
        /*LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));*/

    }

    @Override
    protected void onStop() {
        super.onStop();
        finishAndRemoveTask();
    }


    private Location getCurrentLocation() {
        Location location = null;

        LocationManager locationManager =
                (LocationManager) getApplicationContext().getSystemService(getApplicationContext().LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        return location;
    }
}
